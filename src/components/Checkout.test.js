import React from 'react';
import {shallow} from 'enzyme';
import Checkout from './Checkout';

describe('<Checkout />', () => {
    it('renders input', () => {
        const input = shallow(<Checkout />);
        expect(input.find('input'));
    })
});
