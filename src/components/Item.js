import React, { Component } from 'react';
import './style.scss';
import {Link} from 'react-router-dom';
import {ItemsConsumer} from '../context';
import PropTypes from 'prop-types';

export default class Item extends Component {
  render() {
    const{id, title, image, price} = this.props.item;
    return (
      <div className="col-9 mx-auto col-md-6 col-lg-3 my-3">
        <div className="item-box p-5">
        <ItemsConsumer>
          {(value) => (
          <div className="image-box p-2" onClick={() =>
          value.handleDetail(id)
          }>
          <Link to="/details">
            <img src={image} alt="image" className="card-img-top" />
          </Link>
          <button className="cart-button" onClick={() => {
            value.toCart(id);
          }}>
          <i className="fas fa-cart-plus" /> To cart
          </button>
        </div>
          )}

        </ItemsConsumer>
        
          <div className="box-bottom d-flex justify-content-between">
          <p className="align-self-center mb-0">{title}</p>
          <h5>{price}€</h5>
          </div>
        </div>
      </div>
    )
  }
}

Item.propTypes = {
  item:PropTypes.shape({
    id:PropTypes.number,
    image:PropTypes.string,
    title:PropTypes.string,
    price:PropTypes.number
  }).isRequired
};