import React, { Component } from 'react';
import {ItemsConsumer} from '../context';
import {Link} from 'react-router-dom';

export default class Details extends Component {
  render() {
    return (
      <ItemsConsumer>
        {value => {
          const {id, title, image, price, description} = value.detailProduct;
          return (
            <div className="container py-5">
            <div className="row">
            <div className="col-10 mx-auto text-center my-5">
            <h1>{title}</h1>
            </div>
            </div>
            <div className="row">
            <div className="col-10 mx-auto col-md-6 my-3">
            <img src={image} className="img-fluid" alt="" />
            </div>
            <div className="col-10 mx-auto col-md-6 my-3">
            <h5 className="desc">{description}</h5>
            <h3><strong>{price}€</strong></h3>
              <Link to="/">
              <button className="btn back-to-products">
              Back to products
              </button>
              </Link>
            <button className="btn to-cart" 
              onClick={() => {
              value.toCart(id);
            }}>
              Add to cart
            </button>
            </div>
            </div>
            </div>
          )
        }}
      </ItemsConsumer>
    );
  }
}
