import React, { Component } from 'react';
import Item from './Item';
import Name from './Name';
import {ItemsConsumer} from '../context';
import Chat from './Chat';

export default class extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="py-5">
          <div className="container">
            <Name name="Fruits" />
            <div className="row">
            <ItemsConsumer>
              {value=>{
                return value.items.map(item => {
                  return <Item key={item.id} item={item}/>;
                })
              }}
            </ItemsConsumer>
            </div>
              <Chat />
          </div>
        </div>
      </React.Fragment>
        // <Item />
    );
  }
}
