import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import logo from '../images/logo.png';
import './style.scss';

export default class Navigation extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm">
        <Link to="/">
          <img className="logo" src={logo} alt=''/>
        </Link>
        <ul className="navbar-nav align-items-center">
        <li className="nav-item"></li>
        <Link to="/" className="nav-link">
          Products
        </Link>
        </ul>
        <Link to="/cart" className="ml-auto">
          <button className="cart-button">
            <i className="fas fa-cart-plus" />
          </button>
        </Link>
      </nav>
    )
  }
}
