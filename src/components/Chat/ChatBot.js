import React, { Component } from 'react';
import './chat.scss';

class ChatBot extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        updated: true,
        currentStep: 0,
        inputError: false,
        errorMessage: false,
        dialogs: [],
        steps: [
          {
            id: 0,
            msg: 'Hello, How can I help you today?',
            user: true,
            seen: false,
            options: [
              {
                option: 'I cant find the cart',
                chosen: false,
                next: 1
              }, {
                option: 'How can I pay?',
                chosen: false,
                next: 2
              }
            ]
          }, {
            id: 1,
            msg: 'It is the icon on the right corner of the website',
            user: false,
            seen: false,
            end: true
          }, {
            id: 2,
            msg: 'In the cart, push the checkout button',
            user: false,
            seen: false,
            end: true
          }
        ],
        render: false,
        ended:false,
      };
    }
    componentDidMount() {
      setTimeout(function() {
        this.setState({render: true})
      }.bind(this), 7000)
  
    }
  
    componentWillMount() {
      let currentDialog = [...this.state.dialogs];
      let msg = {};
      msg['text'] = this.state.steps[this.state.currentStep].msg;
      msg['party'] = 'bot';
      currentDialog.push(msg)
      this.setState({dialogs: currentDialog})
    }
  
    componentDidUpdate() {
      if (this.state.updated === true) {
  
        setTimeout(function() {
          this.setState({updated: false})
        }.bind(this), 300)
      }
      if (this.state.inputError === true) {
        setTimeout(function() {
          this.setState({inputError: false})
        }.bind(this), 1000)
      }
      if (this.state.errorMessage === true) {
        setTimeout(function() {
          this.setState({errorMessage: false})
        }.bind(this), 6000)
      }
      if(this.state.steps[this.state.currentStep].end === true && this.state.ended === false){
    this.setState({ended:true});
  
     }
    }
  
    showOptions() {
      if (this.state.steps[this.state.currentStep].options !== undefined) {
        return (this.state.steps[this.state.currentStep].options.map((option, i) => {
          return (<UserOption className={this.state.updated === true
              ? 'updated'
              : ''} key={i} option={option.option} handleButtonClick={() => this.handleButtonClick(option)}/>)
        }))
      }
  
    }
  
    handleButtonClick = (option) => {
  
      let selectedOption = option.next;
      this.setState({currentStep: selectedOption});
      this.setState(state => {
        state.currentStep = selectedOption;
      }, () => {
        let currentDialog = [...this.state.dialogs];
        let msg = {};
        let msg2 = {};
        msg['text'] = option.option;
        msg['party'] = 'user';
        msg2['text'] = this.state.steps[this.state.currentStep].msg;
        msg2['party'] = 'bot';
        currentDialog.push(msg, msg2);
        this.setState({dialogs: currentDialog})
        this.setState({updated: true})
      });
    }
  handleReload = () => {
  
    this.setState(state => {
      state.ended = false;
      state.currentStep = 0;
    state.dialogs = [];
    }, () => {
      let currentDialog = [...this.state.dialogs];
      let msg = {};
      msg['text'] = this.state.steps[this.state.currentStep].msg;
      msg['party'] = 'bot';
      currentDialog.push(msg)
      this.setState({dialogs: currentDialog})
    });
  
  
  }
    handleKeyPress = (e) => {
      if (e.key === 'Enter') {
        let chosenAnswer = e.target.value.toUpperCase();
        this.state.steps[this.state.currentStep].options.map((option, i) => {
  
          if (chosenAnswer === option.option.toUpperCase()) {
            let selectedOption = option.next;
            this.setState({currentStep: selectedOption});
            this.setState(state => {
              state.currentStep = selectedOption;
            }, () => {
              let currentDialog = [...this.state.dialogs];
              let msg = {};
              let msg2 = {};
              msg['text'] = option.option;
              msg['party'] = 'user';
              msg2['text'] = this.state.steps[this.state.currentStep].msg;
              msg2['party'] = 'bot';
              currentDialog.push(msg, msg2);
              this.setState({dialogs: currentDialog})
              this.inputEntry.value = "";
              this.setState({updated: true})
              if (this.state.inputError === true) {
                this.setState({inputError: false})
              }
              if (this.state.errorMessage === true) {
                this.setState({errorMessage: false})
              }
            });
          } else {
  
            this.setState({inputError: true})
            this.setState({errorMessage: true})
  
          }
  
        })
  
      }
  
    }
  
    render() {
      return (<div className={`Chat ${this.state.render
          ? 'visible'
          : 'loading'}
          ${this.state.inputError
            ? 'inputError'
            : ''} ${this.state.errorMessage
              ? 'errorMessage'
              : ''}
              ${this.state.ended
                ? 'ended'
                : ''}`}>
  
        {
          this.state.dialogs.map((dialog, i) => {
            return (<ChatMsg key={i} msg={dialog.text} className={dialog.party === 'user'
                ? 'user'
                : 'bot'}/>)
          })
        }
  
        {this.showOptions()}
        <span className="error-message">Click one of the options or type the option above</span>
      <input ref={el => this.inputEntry = el} disabled={this.state.ended === true ? 'disabled':''} type="text" onKeyPress={this.handleKeyPress}/>
    <div className="reload" onClick={this.handleReload}><span>Restart</span></div>
    </div>);
  
    }
  }
  class UserOption extends React.Component{
    render(){
      return(
        <div className={`UserOption ${this.props.className}`}>
          <p className="p-chat" onClick={this.props.handleButtonClick}>
            {this.props.option}
          </p>
        </div>
      );
    }
  }
  class ChatMsg extends React.Component{
    render(){
      return(
        <div className={'ChatMsg '+this.props.className}>
          <p className="p-chat">
            {this.props.msg}
          </p>
        </div>
      );
    }
  }
  
export default ChatBot;
  