import React from 'react';
import './cart.scss';

export default function CartItem({item,value}) {
    const{id, title, image, price, total, count} = item;
    const{increment, decrement, removeItem} = value;
  return (
    <div className="row my-2 text-center">
        <div className="col-10 mx-auto col-lg-2">
            <img 
            src={image} 
            style={{width:'5rem',height:'5rem'}} 
            classname="img-fluid" alt="" 
            />
        </div> 
        <div className="col-10 mx-auto col-lg-2">
            <span className="d-lg-none">Product: </span>
            {title}
        </div>
        <div className="col-10 mx-auto col-lg-2">
            <span className="d-lg-none">Price: </span>
            {price}
        </div>
        <div className="col-10 mx-auto col-lg-2 my-2 my-lg-0">
            <div className="d-flex justify-content-center">
                <div>
                    <span className="btn mx-1 plus-minus" onClick={() => 
                    decrement(id)}>-</span>
                    <span className="btn mx-1">{count}</span>
                    <span className="btn mx-1 plus-minus" onClick={() => 
                    increment(id)}>+</span>
                </div>
            </div>
        </div>
        <div className="col-10 mx-auto col-lg-2">
            <div className="cart-icon" onClick={() => removeItem(id)}>
            <i className="fas fa-trash"></i>
            </div>
        </div>
        <div className="col-10 mx-auto col-lg-2">
            <strong>{total}€</strong>
        </div>
    </div>
  );
}
