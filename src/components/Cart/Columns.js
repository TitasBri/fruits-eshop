import React from 'react'

export default function Columns() {
  return (
    <div>
          <div className="container-fluid text-center d-none d-lg-block">
      <div className="row column">
      <div className="col-10 mx-auto col-lg-2">
      <div>
          <p className="column">Products</p>
      </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
      <div>
          <p className="column">Name</p>
      </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
      <div>
          <p className="column">Price</p>
      </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
      <div>
          <p className="column">Quantity</p>
      </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
      <div>
          <p className="column">Delete</p>
      </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
      <div>
          <p className="column">Total</p>
      </div>
      </div>
      </div>
    </div>
    </div>
  )
}
