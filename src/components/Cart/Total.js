import React from 'react';
import {Link} from 'react-router-dom'

export default function Total({value}) {
    const {cartTotal, clearCart} = value;
  return (
    <React.Fragment>
        <div className="container">
            <div className="row">
                <div className="col-10 mt-2 ml-sm-5 ml-md-auto col-sm-8 text-right">
                    <Link to='/'>
                    <button className="btn btn-outline-danger px-5 mb-3" type="button" onClick={() => clearCart()}>
                    Clear Cart
                    </button>
                    </Link>
                    <h5 className="total">
                        <span>
                            Total price: {cartTotal}€
                        </span>
                    </h5>
                    <Link to='/checkout'>
                    <button className="btn btn-outline-success px-5 mb-3" type="button">
                    Checkout
                    </button>
                    </Link>
                </div>
            </div>  
        </div>
    </React.Fragment>
  );
}
