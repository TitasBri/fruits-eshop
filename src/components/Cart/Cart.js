import React, { Component } from 'react';
import Name from '../Name';
import Columns from './Columns';
import Empty from './Empty';
import {ItemsConsumer} from '../../context';
import List from './List';
import Total from './Total';

export default class Cart extends Component {
  render() {
    return (
    <section>
        <ItemsConsumer>
            {value => {
                const{cart} = value;
                if (cart.length > 0){
                    return (
                        <React.Fragment>
                            <Name name="Shopping Cart" />
                            <Columns />
                            <List value={value} />
                            <Total value={value} />
                        </React.Fragment>
                    );
                } else {
                    return (
                    <Empty />
                    );
                }
            }}
        </ItemsConsumer>
    </section>
    )
  }
}
