import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

export default class Thanks extends Component {
    state = {
        redirect: false,
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState({
                redirect: true,
            })
        }, 3000)
    }
  render() {
    if (this.state.redirect) {
        return (
            <Redirect to={'/'} />
        )
    }
          return (
            <div>
                <div className="success-icon">
                    <i class="far fa-check-circle fa-10x"></i>
                </div>
                <div className="thank-text">
                    <p className="text-p">Thanks you for your order</p>
                </div>
            </div>
    );
  }
}
