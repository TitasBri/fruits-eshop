import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class Checkout extends Component {
  render() {
          return (
              <div className="checkout-container">
                <div class="col-md-4 container bg-default">
                <h4 className="text-center my-5">
					Checkout form
			</h4>
                    <form>
                    <div class="form-row">
                        <div class="col-md-5 mx-3 my-3 form-group">
                            <label for="firstname">First Name</label>
                            <input type="text" class="form-control" id="firstname" placeholder="First Name"/>
                            <div class="invalid-feedback">
                                Valid first name is required.
                            </div>
                        </div>

                        <div class="col-md-5 mx-3 my-3 form-group">
                            <label for="lastname">Last Name</label>
                            <input type="text" class="form-control" id="lastname" placeholder="Last Name"/>
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
					    <div class="col-md-5 mx-3 my-3 form-group">
							<label for="card-name">Name on card</label>
							<input type="text" class="form-control" id="card-name" placeholder required/>
							<div class="invalid-feedback">
								Name on card is required
							</div>
						</div>

						<div class="col-md-5 mx-3 my-3 form-group">
							<label for="card-no">Card Number</label>
							<input type="text" class="form-control" id="card-no" placeholder required/>
							<div class="invalid-feedback">
								Credit card number is required
							</div>
						</div>
				    </div>

				    <div class="row mt-4">
					    <div class="col-md-5 mx-3 my-3 form-group">
							<label for="expiration">Expire Date</label>
							<input type="text" class="form-control" id="card-name" placeholder required/>
							<div class="invalid-feedback">
								Expiration date required
							</div>
						</div>
					
					    <div class="col-md-5 mx-3 my-3 form-group">
							<label for="ccv-no">Security Number</label>
							<input type="text" class="form-control" id="sec-no" placeholder required/>
							<div class="invalid-feedback">
								Security code required
							</div>
					    </div>
				    </div>
                    <Link to="/thanks">
                    <button class="btn btn-primary btn-block buy-btn" type="submit">Buy</button>
                    </Link>
                </form>
            </div>
        </div>
    );
  }
}
