import React, { Component } from 'react';
import './style.scss';
import ChatBot from '../components/Chat/ChatBot';

class Chat extends Component {
   constructor(props) {
      super(props);
      
      this.state = { modal: false }
   }
   
   showModal() {
      this.setState({ modal: true });
   }
   
   closeModal() {
      this.setState({ modal: false });
   }
   
   render(){
      return (
         <div className="chat"> 
            <button 
               onClick={this.showModal.bind(this)}
               type='button'
               className='btn chat-btn'>
               Chat with us!
            </button>
            <Modal isShown={this.state.modal} onClose={this.closeModal.bind(this)} />
         </div>
      );
   }
}

const Modal = props => {      
   let popUp = props.isShown ? {display: 'block'} : null;
   
   return(
      <section id='modal' className='modal fade show' style={popUp}>
         <div className="modal-dialog" role="document">
            <div className="modal-content">
               <header className='modal-header'>
                  <h5 className='modal-title'></h5>
                  <button 
                     onClick={props.onClose}
                     type='button'
                     className='btn'>
                        <i class="fas fa-times"></i>
                  </button>
               </header>
               <article className='modal-body'>
                  <ChatBot />
               </article>
            </div>
         </div>
      </section>
   );
}

export default Chat;