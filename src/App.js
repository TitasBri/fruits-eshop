import React from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from './components/Navigation';
import Items from './components/Items';
import Details from './components/Details';
import Cart from './components/Cart';
import Checkout from './components/Checkout';
import Thanks from './components/Thanks';

function App() {
  return (
    <React.Fragment>
      <Navigation />
      <Switch>
        <Route exact path="/" component={Items}></Route>
        <Route path="/details" component={Details}></Route>
        <Route path="/cart" component={Cart}></Route>
        <Route path="/checkout" component={Checkout}></Route>
        <Route path="/thanks" component={Thanks}></Route>
      </Switch>
    </React.Fragment>
  );
}

export default App;
