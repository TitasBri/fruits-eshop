import React, { Component } from 'react'
import {productsData, detailProduct} from './data';

const ItemsData = React.createContext();

class ItemsProvider extends Component {
    state = {
        items: [],
        detailProduct: detailProduct,
        cart: [],
        cartTotal: 0
    };
    componentDidMount(){
        this.setItems();
    }
    setItems = () => {
        let tempItems = [];
        productsData.forEach(item => {
            const oneItem = {...item};
            tempItems = [...tempItems, oneItem];
        });
        this.setState(() => {
            return {items: tempItems}
        });
    };
    getItem = (id) => {
      const product = this.state.items.find(item => item.id === id);
      return product;
    };
    handleDetail = (id) => {
        const product = this.getItem(id);
        this.setState(() => {
          return {detailProduct:product};
        })
    };
    toCart = id => {
        let tempItems = [...this.state.items];
        const index = tempItems.indexOf(this.getItem(id));
        const item = tempItems[index];
        item.count = 1;
        const price = item.price;
        item.total = price;
        this.setState(() => {
          return  {items: tempItems, cart:[...this.state.cart, item]};
        },()=>{
          this.addTotal();
        })
    };

    increment = (id) => {
      let tempCart = [...this.state.cart];
      const select = tempCart.find(item => item.id === id);
      const index = tempCart.indexOf(select);
      const item = tempCart[index];

      item.count = item.count + 1;
      item.total = item.count * item.price;

      this.setState(() => {
        return {
          cart:[...tempCart]
        }
      },() => {
        this.addTotal()
      })
    }

    decrement = (id) => {
      let tempCart = [...this.state.cart];
      const select = tempCart.find(item => item.id === id);
      const index = tempCart.indexOf(select);
      const item = tempCart[index];

      item.count = item.count - 1;

      if(item.count === 0){
        this.removeItem(id)
      } else {
        item.total = item.count * item.price;
        this.setState(() => {
          return {
            cart:[...tempCart]
          }
        },() => {
          this.addTotal()
        })
      }
    };

    removeItem = (id) => {
      let tempItems = [...this.state.items];
      let tempCart = [...this.state.cart];

      tempCart = tempCart.filter(item => item.id !== id);

      const index = tempItems.indexOf(this.getItem(id));
      let removed = tempItems[index];
      removed.count = 0;
      removed.total = 0;

      this.setState(() => {
        return {
          cart:[...tempCart],
          items:[...tempItems]
        }
      },() => {
        this.addTotal();
      })
    }

    clearCart = () => {
      this.setState(() => {
        return {
          cart:[]
        };
      })
    }

    addTotal = () => {
      let subTotal = 0;
      this.state.cart.map(item => (subTotal += item.total));
      this.setState(() => {
        return {
        cartTotal: subTotal
        }  
      })
    }
  render() {
    return (
      <ItemsData.Provider value={{
        ...this.state,
        handleDetail:this.handleDetail,
        toCart:this.toCart,
        increment: this.increment,
        decrement: this.decrement,
        removeItem: this.removeItem,
        clearCart: this.clearCart
      }}>
        {this.props.children}
      </ItemsData.Provider>
    )
  }
}

const ItemsConsumer = ItemsData.Consumer;

export {ItemsProvider, ItemsConsumer};